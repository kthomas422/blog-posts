# Learning Git as Early as Possible

To pile onto the list of things to learn when learning to code, [git](https://git-scm.com) is a pretty good one to learn as early as possible.
Git is a version control system, it's a powerful tool for keeping a history of your project changes and also for collaborating with other
people. There are several sites that host git repositories (code projects) and you can collaborate with others and host your projects.
[GitHub](https://github.com) is the most popular one but [GitLab](https://gitlab.com), [Bitbucket](https://bitbucket.org),
[AWS](https://aws.amazon.com/codecommit/), [Google Cloud](https://cloud.google.com/source-repositories/docs), and
[Azure](https://azure.microsoft.com/en-us/products/devops/repos) also have  hosting for git. My favorite is [GitLab](https://gitlab.com)

Git is great when starting out because you can experiment more without risking losing your progress or if you get your code into such a bad
state that you can revert it back to the working state. If you have someone experienced with programming willing to help you, then it will be
much easier for the person to help you out as well.

## Quick Git Tutorial

Below I'll go over how to quickly get setup and running with git. There's way more it can do, but this is all you need to get started.

### Installing

Windows: Open up powershell and type `winget install -e --id Git.Git`

Or you can just go to https://git-scm.com/downloads and install it from there.

### Setup

Before you start using git there are some options you want to set:

Set the username: `git config --global user.name "Your Name"`

Set email: `git config --global user.email "youremail@example.com`

Set default branch: `git config --global init.defaultBranch main`

Delete references to merged branches on remote: `git config --global fetch.prune true`

Auto set remote on push: `git confg --global push.autoSetupRemote true`

### Usage

#### New Project

On a project without git setup yet (can have code in it or be an empty directory):

`git init`

Or if you created a project on something like GitLab first, then follow the instructions it gives you.

#### Create a new branch to work on

You can work directly off the main branch too.

`git checkout -b <branch name>`

#### Committing your work

```
git add file1 file2 ... filen

git commit -m "a short message of what you changed"`
```

If you already setup a remote following the instructions from GitLab then you can also push your changes:

`git push`

#### Git Status

Between "adding" and "committing" your changes the `git status` command will let you know what changes are going to happen as
well as what branch you're on and how far your local copy and the remote repository has drifted.

You can also view the changes that are going to be committed after you've added them by doing `git diff --staged`.

#### Un-staging

If you've added the files but haven't committed yet and you want to commit without an added file you can "unstage it" by running
`git restore --stagged file1 file2 ... filen`

#### Restoring files

If you want to restore a file you changed back to its last committed state you can run `git restore file1 file2 ... filen`.

## Wrap-up

There's much more to git than that, but that's the basics (and then some) to get you started. Atlassian has some amazing articles to tutorials
to learn some more. https://www.atlassian.com/git

