# Docker Images from Targeted Stages

Recently, while reading through the [docker docs](https://docs.docker.com/engine/reference/commandline/build/#options) I stumbled upon a
`--target` argument. This stops building the container once it reaches the targeted stage in the Dockerfile. I was intrigued and checked
the [kaniko docs](https://github.com/GoogleContainerTools/kaniko) and sure enough, it has the same thing. This was important because we use
Docker to build images locally for testing, but in our GitLab pipelines we use Kaniko to build our images.

This wasn't the current problem I was trying to solve at work, but it did solve a major headache we had, so I paused my current task to
test this out. We have microservices written in PHP Laravel and they all use a base container that has composer, PHP, SQL drivers, etc.
Then each microservice has its own image that uses this image to build off. We also have a PHP development image based off this
base image. This adds in things like `XDEBUG`, `vim`, `iputils` and anything else that's handy for troubleshooting. We also have each
microservice build off this development image. The non-development image gets deployed to our AWS cluster for our customers as well as
our on-premises production cluster created with RKE. The development version gets deployed to our on-premises development cluster, also
created with RKE.

The problem that setting the build target solved is that before this our pipeline built the base image first, and then
in a separate job built the development image after the base image was finished. This was slow because we were waiting for the base image
to be made and then pushed to the repository, then pulled back down and having the added dependencies put in. It also created some
chicken or egg problems for us when we wanted to test things.

The target flag fixes some of these problems (and creates some, everything is a tradeoff). Now we just have one
Dockerfile for the base image and then tack on a stage at the end that will install the development dependencies. Then, you have your
two images build in parallel. The base image uses the target flag to target the stage before the development tools are installed,
and the development image job builds the entire Dockerfile. This solves our chicken or the egg problem since the development image job
doesn't depend on the base image being finished anymore and it reduced the number of directories and Dockerfiles.

There is one drawback to this approach. You're now building 80% of the same image twice now, since the development one builds the same
base image. But this is worth the cost. Now that both images can be built at the same time, the overall time for both is less because the
overhead of starting the development image job after the base image and pulling the base image took more time. We self-host our runners 
which also have the advantage of full control over the amount of them, their resources, and caching.
