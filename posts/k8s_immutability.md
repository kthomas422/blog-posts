# Treating Kubernetes Nodes as Immutable

Twice lately we've learned at work that it isn't worth making major changes to a Kubernetes node, and you should 
create a new node instead.

Creating and adding nodes to a cluster is extremely cheap. Especially if you're using GitOps. At my work we have an on premises
Kubernetes cluster provisioned by Rancher and RKE. The nodes are Ubuntu server VM's on VMware ESXI. We have templates for
the nodes (now haha) so IT can hand our development team a fresh node to the cluster very quickly. (Where I work, IT handles
VMware and the developers manage the cluster and workloads).

---

The first story that taught me to treat these Ubuntu servers as immutable was when Ubuntu 18.04 reached EoL. Half of our nodes
were on 18.04 and the other half was 20.04. We decided to update the 18.04 nodes to 22.04 and leave the 20.04 to have some
diversity in the cluster. The node updates went smoothly, we updated one at a time and the workloads looked like they were
running perfectly. Until later that afternoon we started getting emails about services being down. This was a Friday afternoon when
all the other developers had stopped working for the weekend as well as most of IT. I was unable to find the problem in a quick
 manner, and the more time passed, the more hosed the cluster was becoming. So I had IT restore the backups for me that they had
taken before upgrading the Ubuntu version.

The next week we tried again thinking I fat-fingered a config override when `do-release-upgrade` prompted me. That was hopeful
thinking but this time I had more developers to help, and we determined that there was some change to the way Ubuntu handled DNS
and it wasn't carrying over to Kubernetes' CoreDNS. We went pretty deep trying to solve the problem but were unable to. Instead,
we had IT create us some new Ubuntu 22.04 nodes and added those to the cluster. But those had the same problem. We finally
figured out it was a DNS setting in Ubuntu, but by then the former 18.04 nodes were "hammed on" as we call it, so we still removed those nodes
and the new 22.04 nodes from the cluster, and then created new 22.04 nodes from the templates, but followed our documented
procedure for fixing the problem with fresh nodes.

We're a fabrication company and only have a two pizza development team. One other person and I handle almost all the DevOps.
Every time we look at the cost of implementing IaC for the cluster it doesn't seem worth it since we rarely do any major changes,
especially with our higher priority development projects.

---

The second and final lesson about treating your nodes as immutable was when there was a power outage at work. We've had power
outages before, but this one was long enough for the battery backups to go dead bringing our cluster down. When the cluster came
back up our HA Proxy and a few nodes had changed IP addresses. We were under the assumption all the leases were static but
apparently not. We were able to remove the nodes from the cluster and then re-add them with their new IP addresses and get HA Proxy
reconfigured. Then IT decided to move the cluster to its own subnet with static IPs instead of leases.

Thinking it would be a breeze to move the cluster since we just did it with a few nodes, we began changing them over to the new
subnet with static IPs instead of DHCP leases. This went horribly wrong and forced us to rebuild the entire cluster from scratch.
Luckily, the company mostly uses our legacy site, which was still up, but it is definitely described as the week from hell. Everything
on that cluster was deployed by multiple people through the Rancher web application over the years. We were testing GitOps on our development
network, but it wasn't quite ready to implement on our production cluster.

Thankfully, we had pulled down all the yaml manifests of our application stacks before ruining the cluster. So once we took a day
to get a new cluster up and running, we then installed the GitLab Agent and Flux in the cluster and went to work cleaning up the
yaml and checking it into version control. Originally, the cluster was created as an experiment, trying out the newest tools in 2019.
Back when there were only two or three software engineers and one web application. When it blew up and had to be rebuilt, it had
grown to several microservices we wrote and several other open-source applications the company relied on. We essentially spent the
entire week paying off the technical debt that had been accumulating inside that cluster over four years.

---

So now if our cluster ever goes again, we can have all of our services redeployed in minutes since we have GitOps now. We also have
documented procedures for removing and adding nodes to the cluster instead of treating the nodes like the wild west. We're getting
newer equipment on our development network too so that we can begin to try out [Terraform](terraform.io) and
[Talos Linux](https://www.talos.dev/) so the Kubernetes infrastructure is under version control as will as our deployments.

