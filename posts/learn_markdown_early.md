# Learn Markdown as Early as Possible

Learning to code can be quite overwhelming. So why would anyone add something else like Markdown to the list of things to learn?
I'm not saying you should learn Markdown right away. However, once you have a good grasp of your first language down and possibly some
git commits under your belt, then learning Markdown is a valuable addition to your programming journey.

## What is Markdown?

[Markdown](https://www.markdownguide.org/) is a markup language like HTML (Hyper Text Markup Language). In simple terms, this means it's a format that's both human and machine-readable.
You create Markdown files in a text editor like notepad and save it with a `.md` extension.

You can share this file and people can read it in the *raw* form, or you put it on various sites like [GitLab](https://gitlab.com),
[Confluence](https://www.atlassian.com/software/confluence) or even inside [Slack](https://slack.com) and it will
render it in a nice format.

## Why Markdown?

The biggest reason is documentation for your code projects, starting with the readme. With very few exceptions, every project
should have one and most of the git hosting services like GitLab will automatically render it on your project page. In your first few projects, these readme's will
mostly be for helping you remember when you revisit your project since you will have few collaborators, but the habit is good to start.

Our team maintains a knowledge base using Confluence to try and reduce the amount of tribal knowledge. Tools like Confluence work great with Markdown syntax.
Confluence is an amazing tool. Not only does it render the Markdown for you while you make the document and others view it, but it also has a feature to
export the document as PDFs or even M$ Word documents to share.

Another great use case is for communicating through chat applications like Slack or Teams. These and most others support Markdown syntax. Sure, you can use the
formatting buttons they have, but then take your hands off of the keyboard and click on the icons, but that slows you down. The most common use case for
this is for making `code sections` with back ticks \`\` or code blocks for pasting in longer code sections and logs. They're much easier to read for others if
they're formatted like this. And please use three back ticks to make a block if what you're sending is lengthy, otherwise it's worse trying to read
that than if it wasn't put in a code block at all.

As mentioned earlier, you can even translate Markdown into HTML. These blog posts are written in Markdown and then pushed to a git repository. Then
[thomaspanacea.com](https://thomaspanacea.com) automatically grabs each post when someone visits and displays it as an HTML article to the reader.

A more obscure use case is email. There's a browser extension called [Markdown Here](https://markdown-here.com/) that will convert your email into Markdown. This
works because email clients can render HTML.

Most of these use cases you could just use "the buttons" in the GUI or HTML, but Markdown is much simpler for you to type than HTML and vastly easier to read. If
you gave a lay person an unrendered Markdown file, they could easily read it which isn't the case for HTML.

The last pro for Markdown I'll elaborate on, is since its plaintext it pairs extremely well with git. This makes collaboration very efficient. Several people can
edit the same document (or set of documents) and not only have it version controlled but also take advantage of branching and peer review.

## Convinced?

Hopefully I've convinced you of the need to learn this simple, yet powerful communication tool. Don't put off things that matter or overwhelm yourself with another
thing to learn when you're learning how to code. But as soon as you get a good chance or use case to learn Markdown, then I can't recommend it enough.

Someone can learn Markdown in the time it takes them to make one document. Just open up notepad or an IDE alongside the [cheat sheet](https://www.markdownguide.org/cheat-sheet/) and start typing.
