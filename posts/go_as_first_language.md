# The Case for GoLang as Your First Language

Before I make the case that Go would be a great first programming language to learn I want to make it clear that in my opinion, and many
others, that if you are already learning to code in something else like Python then you stick with it and don't fall into the trap of
language hopping.

With that out of the way let me introduce [GoLang](https://go.dev). Go is a simple language that is strong and statically typed (more on this
later), compiled and garbage collected (again more on these later). It was made as a microservice backend language but also lends itself
well to making command line applications. Lots of my favorite tools like [Docker](https://docker.com), [Kubernetes](https://kubernetes.io/)
and [Hashicorp](https://www.hashicorp.com/) products are written in Go. At work, I was the project manager for a systems application to synchronize
a third party database with our own using Go.

I'll be contrasting Go with Python a lot in this article. That's because Python is a very popular first language. I use Python quite a bit
at work and love it for small scripts. However, for reasons we'll get into I'm not a fan of Python for large applications. It's been done
successfully by people a lot smarter than me, so take my opinion with a grain of salt, but I don't like working on large projects in
something like Python where the types are dynamic, and data encapsulated inside objects can be changed by anything at any time.

## Brackets to Deliminate Blocks

Go's programming blocks are put inside curly brackets `{}` like the C family of languages while Python uses tabs. Most languages you'll
encounter use brackets and for good reason. It's easy to read/see where a block starts and ends. The tab delimitation in Python can
cause you to introduce logic bugs more easily since you can accidentally have `if` statements on the wrong indentation. A similar benefit
that Go has over Python in this regard is its way easier (and expected) to run the Go Format tool every time you save your file. This fixes
any indentation and other common code formatting errors. Go is very opinionated on how to format it which keeps all Go code consistent.

## Strong and Static Typing

Go is strongly and statically typed. In simple terms, you must define what type your variables are and you can't mix types. For example,
if you have a function expecting an integer (a number), then you cannot pass a string (text) into the function. Other languages like PHP
will attempt to convert the string into an integer for you. Go will fail to compile. You can't even add a 32-bit integer with a 16-bit
integer in Go, you'll have to cast one to the other. You also can't have a variable as an integer and then later change it to a string.
This may seem annoying when Python will happily let you do these things, but as you work on larger projects or jump into unfamiliar
code bases then the typing system will be a godsend to you. Good typing systems are so much better for maintainable code it's the reason
IDE's like PyCharm will hint at the type of Python variables in functions, why TypeScript exists and why every new major version of PHP
gets more and more strongly typed. At work we can't upgrade our PHP version on our legacy application because it relies on "undefined
behavior" with how different types interact with each other.

## Compiled

This leads us nicely into the next reason Go is a great first language, the compiler. A compiler takes your code which is human-readable
and creates a binary file that the computer runs. An alternative to this is an interpreted language like Python where it converts the code
to the machine code as it runs. The Go compiler isn't as good as Rust's compiler, but once
your code compiles you can be sure most typing bugs won't exist during runtime. In Python, you can get an error during runtime
by passing the wrong type into an API or library. In Go the compiler will catch this kind of error, and you'll have to fix it before you
even run the code. The Go compiler will also not let you have unused variables, imports and other things that can bloat your code or remind
you to use a variable you intended to use (like an error). In addition to catching errors, having a compiled application is convenient for
code deployment and distribution. Once compiled you can usually put the binary on any computer with the same OS and CPU architecture
and it will run. With an interpreted language like Python, the python files can be copied over but python will have to be installed on that
computer for it to run. Put another way, you don't need Go installed on a computer to run Go applications, but you do need Python or PHP
installed to run those applications.

Another great reason for a compiled language like Go over Python is its much faster. For most applications, especially IO bound applications,
most won't notice this, but when you're doing something computationally heavy or iterating over large lists then it will make a big difference.

A disadvantage of the compiler is it slightly slows down development since you must compile and then run your application versus just
running it. And if you are stupid like I am, sometimes you forget to recompile your code after a change and then spend too much time wondering
why the changes aren't in there. However, Go works around this (and many other modern compilers) by having a run command. `go run <go file>`
will execute your Go code just like `python <python file>` will.

## Simple

Go as designed for recent CS graduates to pick up quickly. It's commonly described as a combination of C and Python. There are only 25 reserved
keywords in Go, Python is also pretty light at 33 but PHP and C++ have more than 70! This isn't the whole story, but without syntax sugar and
extraneous features Go is much simpler which makes for an easier learning curve.

Unlike most of the popular languages, Go doesn't have exceptions for error handling. Instead, functions will return an error value. Not
having exceptions and just returning a value to check is much simpler and has the
added benefit of making it explicit when you don't check for an error. In exception languages you need to look at documentation or the code
what you're calling to see if it throws an exception or wrap large swaths of your code in a try/catch block which is an anti-pattern. This 
lowers the maintainability of the code long term. While it is "simpler" not having exceptions, in my experience you do get much more
boilerplate code in Go than other languages.

Go only has one keyword for loops which is ironically the `for` loop. Most have a `for` and a `while` but Go just uses one keyword. I'm not
entirely convinced this makes it much simpler but it was an attempt. In Go to loop for forever you just have `for { ... }`, if you want to
loop until a condition is met you do `for <condition> { ... }`, iterate over an array: `for index, value := range myArray { ... }`, plus a
traditional C loop.

The way Go handles packages and visibility is also great for simplicity. Python can be obnoxious when learning how modules and importing can
work, but with Go, one directory corresponds to one package. Each Go file has the name of the package as one of the first lines. Package
visibility is determined whether the item starts with a capital or not. This is simple, but the capitalization rule can cause some headaches
instead of just using the `pub` keyword. Our comparison language Python doesn't really have the concept of public or private constructs. This
can really encourage some bad habits of new programmers changing the state of objects anywhere and everywhere. You can still do that with
getters and setters, but at least you could pause to validate your inputs while you write the setter one time.

Go is like C/C++ where you have one entry point into the program, `main`. It doesn't have magic methods or other odd execution paths you need
to learn to know how the program works, it's very sequential.

## Testing

The earlier you start writing unit tests for your code the better. Go does an excellent job with unit tests. It's built into the language and
tools and is just Go code, there aren't any special testing constructs. Bashing on Python some more, it requires to find your own testing
package to install like `pytest` and then adds in assertions (like most languages to be fair). In "normal code" you rarely use assertions
which can make the tests look/feel different from the rest of your code.
In Go tests you usually end up with lots of `if` statements (like your code as well for error handling) and then calling `t.Error()` to make
the test fail. Go tests also work amazingly well for table tests, these tests are amazingly simple, easy to follow and easy to maintain.

## Wrapping Up

If you're still looking around for which programming language to try out as your first one, then I hope you give a Go a good consideration. Its
simplicity, type system and being a compiled language make it a great choice for your first language. Although you can't really go wrong with
any of them, just stick with it and enjoy the journey!

