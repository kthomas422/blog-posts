# LaTeX in 2024

In my Computer Science program we all had this one professor that was old school passionate about systems programming and graphics programming.
I really liked him, he was a great instructor
and had a lot of practical knowledge. One day he mentioned something called [LaTeX](https://latex-project.org) (pronounced la tech). It was off
the cuff and he glossed over it mentioning a good book to read to learn it. After finding out what it was I had to learn it.
If you haven't heard of LaTeX before, it's a typesetting system. You type your document in plain text (so with notepad, vim, an IDE, etc.)
and put "commands" or "markup" in the document, and  then an application will render it in a format of your choosing.
I almost always output PDF documents, but it will do HTML, Postscript and more.

I really detest Word and am so used to using `vim` I get really slowed down using Word or typing emails. I hadn't
heard of Markdown yet so LaTeX seemed really enticing to learn. I bought the recommended
[book](https://www.amazon.com/gp/product/0321173856) and learned it that same semester.

If you're a nerd like me, then LaTeX is still amazing in 2024. You can effortlessly create PDFs of resumes and cover letters and
**version control** it! Being plain text and playing nice with `git` makes LaTeX a great tool to collaborate on. I also write letters of
recommendation and software engineering documents like requirements specifications and design documents with it.
I'm not a mathematician but it formats equations extremely well too. For software engineering documents, I have templates for them created
and version controlled as well.

In addition to the book [Guide to LaTeX](https://www.amazon.com/gp/product/0321173856), the site [overleaf](https://overleaf.com) is a great
resource for learning LaTeX and finding templates. There are several "commands" to create a document. I've had the best results with
`lualatex` and also passing in the `--stop-on-error` flag.
